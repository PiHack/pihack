Nyxt è un browser scritto in Common Lisp.
La scelta del linguaggio lo rende completamente modificabile ed estensibile,
esattamente come emacs (editor di testo scritto in ELisp).
Tuttavia un certo grado di dimestichezza con il linguaggio è necessario
per poter sfruttare a pieno le potenzialità del browser.

# Download
Nyxt si può scaricare dal sito https://nyxt.atlas.engineer/

# Primi passi
La prima cosa da fare è esplorare il tutorial.
Le combinazioni di tasti sono personalizzabili e si può scegliere una disposizione iniziale
consigliata nelle impostazioni.
In generale le impostazioni di default sono sufficienti per un utilizzo non approfondito del browser.

# Personalizzazione ed utilizzo avanzato
Per poter estendere e personalizzare Nyxt è necessario avere una buona dimestichezza con
il linguaggio di programmazione Common Lisp.
PiHack consiglia il seguente [post](https://stevelosh.com/blog/2018/08/a-road-to-common-lisp/).
Il sito [Exercism](https://exercism.org/) propone una pletora di esercizi in CommonLisp e
in diversi altri linguaggi di programmazione: è particolarmente utile se
affiancato ad un libro.

# Conclusioni
Nyxt è ancora in fase di sviluppo e PiHack ha avuto alcune difficoltà che ne hanno
impedito l'utilizzo quotidiano.
Tuttavia l'idea ci piace e continueremo ad esplorarne l'utilizzo.
