---
author: geri
layout: post
published-on: Agosto 2022
---

# Gomme e terminali

L'utilizzo dello *shell scripting* permette
di automatizzare processi altrimenti tediosi.
Navigando su internet mi sono imbattuto in [[https://github.com/charmbracelet/gum|Gum]],
un toolkit che si propone di semplificare
la scrittura di interfacce da linea di comando "glamour"
([[https://en.wikipedia.org/wiki/Command-line_interface|CLI]]).
Ho utilizzato Gum per programmare una
CLI al mio programma di analisi dati
in modo da renderlo quanto più user-friendly
possible.

In questo post descriverò brevemente cosa è
una shell, cosa si intende per shell scripting,
cosa fa Gum e come l'ho usato.

## Shell e script
Una shell è una interfaccia
che permette all'utente o ad altri programmi
l'interazione con il sistema operativo.
Una shell può essere grafica o testuale.
Generalmente quando si parla semplicemente di
"shell"  o "terminale" ci si riferisce ad una
shell testuale con cui l'utente interagisce attrverso
un terminale attraverso una interfaccia a riga di comando.

Nei sistemi operativi Unix e Unix-like esistono diverse
shell testuali. Le più diffuse sono [[https://en.wikipedia.org/wiki/Bash_(Unix_shell)|bash]],
[[https://en.wikipedia.org/wiki/Z_shell|zsh]], [[https://en.wikipedia.org/wiki/Fish_(Unix_shell)|fish]],
ma ne esistono altre.
Una caratteristica di molte shell testuali è la possibilità
di salvare sequenze di comandi per il riutilizzo successivo:
si salvano i comandi in un file
([[https://www.andrewcbancroft.com/blog/musings/make-bash-script-executable/|reso eseguibile]])
e li si fa leggere alla shell che li interpreta e li esegue riga
per riga. Questo è lo *shell scripting*.
Generalmente le shell testuali sono munite di linguaggio
di scripting che permette l'utilizzo di strutture,
condizionali, variabili e altri elementi tipici dei
linguaggi di programmazione. Allo stesso modo alcuni
linguaggi di programmazione possono essere usati interattivamente
da una shell e possono essere usati
per scrivere CLI o TUI [[https://en.wikipedia.org/wiki/Shell_(computing)|[1]]].


## Gum
[[https://github.com/charmbracelet/gum|Gum]] è un toolkit **FOSS**
che fornisce strumenti per la scrittura
di shell script utili e **belli** con poche righe di codice.
Tutti questi strumenti sono altamente configurabili e permettono
di creare delle fantastiche CLI.

Gum è scritto in [[https://go.dev/|Go]] e usa
le librerie [[https://github.com/charmbracelet/bubbles|Bubbles]] e
[[https://github.com/charmbracelet/lipgloss|Lip Gloss]].

Per maggiori info e documentazione la pagina Github è completa di tutto il necessario:
	https://github.com/charmbracelet/gum


## Nella vita vera ci vogliono le gomme
Per l'analisi dati a cui sto lavorando ho scritto
un programma Python articolato con
diverse funzioni che si occupano dei
diversi aspetti dell'analisi.
Eseguivo ciascuna funzione con uno script shell
dedicato.
Questa struttura funzionava abbastanza bene per
me, ma ero consapevole che sarebbe risultata
difficile da comprendere a chiunque altro. Compreso
il me del dopo pausa estiva (illudendomi di averne una).

Vedo il post su Glow pubblicato dal  [[https://mastodon.technology/@charm|profilo Mastodon di Charm]]
e decido di provare ad usarlo per scrivere una interfaccia
utente come si deve per il mio programma di analisi.

### execute.sh
---
*full disclaimer: non sono un programmatore e sono sicuro che tutto*
*si sarebbe potuto fare meglio.*
---

Il primo passo è stato dare all'utente la possibilità di scegliere quale
parte dell'analisi eseguire.
Per farlo ho utilizzato il comando `choose` di Gum:
```bash
 gum choose "last" "abundance" "BGO far" "coincidences" "summing" "strength" "sumpeakfit_abundance"\
	    "test" "none"
```
Ho salvato la scelta in una variabile
```bash
analysis=$( gum choose "last" "abundance" "BGO far" "coincidences" "summing" "strength" "sumpeakfit_abundance"\
	    "test" "none" )
```
e l'ho utilizzata per scegliere lo script da eseguire
```bash
case $analysis in
	"last")
		read script < $chronfile;;
	"abundance")
		script="automate_abundance.sh";;
	"BGO far")
		script="automate_BGOfar.sh";;
	"coincidences")
		script="automate_coinc.sh";;
	"summing")
		script="automate_IO.sh";;
	"strength")
		script="automate_strength.sh";;
	"sumpeakfit_abundance")
		script="automate_sumpeakfit_abundance.sh";;
	"test")
		script="test.sh";;
	"none" | *)
		exit;;
esac
```
dove `chronfile` è il file in cui salvo i parametri dell'ultima
esecuzione: in questo modo posso ri-eseguire velocemente
lo stesso script.

Ho poi chiesto all'utente su quali dati fare l'analisi utilizzando
il comando `confirm`
```bash
gum confirm $style_choice --prompt.margin 1\
	--prompt.foreground "#F4AF23" "Do you wanna choose runs?" && ChooseRuns || gum style\
								      --margin=1 --foreground=$lily\
								      "Ok using default runs"
```
dove `ChooseRuns` è una funzione che ho definito per scegliere i dati da analizzare.
Nel precedente comando si può vedere come ho modificato lo stile
del messaggio di `gum confirm` utilizzando le opzioni `--prompt.foreground`
per il colore del testo e `--prompt.margin` per la distanza dal margine sinistro del terminale.

In `ChooseRuns` ho utilizzato
```bash
gum input --placeholder "Select runs" > "runs.txt"
```
per catturare l'input dell'utente e salvarlo in un file.

Ho infine stampato un resoconto dei dati scelti utilizzando `gum style`
```bash
gum style --margin 1 --foreground "#05ffa1" "Analysing runs\
$(gum style --border thick --margin 1 --foreground "#00b8ff" --border-foreground "#ff00c1" --width 85 --padding "1 2" "$runs")
on targets\
$(gum style --border thick --margin 1 --foreground "#00b8ff" --border-foreground "#ff00c1" --width 85 --padding "1 1" "${targets}")"
```
Ho in particolare utilizzato due `gum style` *nested* per scrivere i dati scelti in maniera diversa dal testo
informativo.

Il resto dello script è esecuzione del codice di analisi.

## Conclusioni
Gum si è dimostrato davvero utile.
In un paio d'ore sono passato da un insieme disunito di
script ad un programma organico supportato da una interfaccia
utente comprensibile e accattivante (accattivante per me che uso solo
il terminale almeno: bianco su nero e si vola.).
