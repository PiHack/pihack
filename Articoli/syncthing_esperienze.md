---
author: geri
layout: post
published-on: 28 Marzo 2022
---

# Esperienze con syncthing

Per lavoro ho avuto necessità di spostare una grande mole di dati da un computer
ad un altro, mantenendo l'abilità di continuare a lavorare sui dati durante la sincronizzazione.
Una ricerca sul web e *soffice* mi hanno indirizzato su [syncthing](https://syncthing.net).

## Cos'è Syncthing?

Syncthing è un software per la sincronizzazione continua di file.
Sincronizza file tra due o più computer in tempo reale
attraverso comunicazioni TLS P2P, ovvero senza necessità di un server centrale.

È un software multi-piattaforma (al momento della scrittura l'unico OS non supportato è IOs) e semplice da usare:
ogni macchina è identificata da un ID unico e la condivisione avviene
attraverso LAN o internet.

## Come lo sto usando?

Dovevo spostare da un computer ad un altro circa ```160 GB``` di
dati per poter lavorare su un computer più potente (lo è davvero?)
e condiviso tra le persone del laboratorio.
Quindi, dopo aver fatto un backup **dell'intero contenuto del mio computer**,
ho inizializzato una cartella syncthing condivisa sul mio PC
e ho copiato in essa i dati da condividere.
Per *paura* non ho copiato tutto insieme, ma ho cominciato con una cartella
da ```10 GB``` di file *recuperabili*.
Dopo aver controllato che tutto fosse andato a buon fine, ho copiato il resto dei
dati nella cartella condivisa e ho atteso.
Ho dovuto ignorare in fase di sincronizzazione le  cartelle ```.git```
per evitare conflitti.
Ho interrotto la condivisione per tornare a casa e l'ho riavviata: non ho perso
niente!
Il mio animo paranoico si è tranquillizzato e continuato la sincronizzazione da casa.
Poiché il segnale wifi in casa è debole
la sincronizzazione ha impiegato diversi giorni per essere completata.

Contento del risultato ho installato Syncthing su un [Raspberry Pi3]()
per avere un backup automatico dei miei file più significativi:
per risparmiare tempo ho copiato offline i dati su un hard disk
che poi ho connesso al Raspberry.
Ho poi aperto la GUI Syncthing sul mio PC e ho inizializzato la cartella
da sincronizzare come cartella *send-only* e ho copiato il suo ID.
Sul Raspberry ho creato una nuova cartella *receive-only* con
lo stesso ID della cartella sul PC.
Ho avviato lo scan delle cartelle su entrambe le macchine.
Terminato lo scan ho condiviso la cartella e, magia magia,
tutto è sincronizzato in tempo reale (o quasi).

## Conclusioni e commenti
Syncthing funziona meglio di quanto mi aspettassi ed è estremamente semplice da usare.
L'aspetto che mi è piaciuto di meno è l'interfaccia web: le operazioni da terminale sono
farraginose e ho trovato poco chiara la loro documentazione.
Il resto delle funzionalità è spiegato in maniera chiara e semplice.
