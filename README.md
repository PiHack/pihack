# Pihack
## Hacklab fisico-digitale

Il 1 marzo 2022 nasce a Napoli il progetto dell'hacklab Pihack.
Il progetto è radicato nel [dipartimento di Fisica dell'Università derli Studi di
Napoli Federico Secondo](https://www.fisica.unina.it).

Sulle orme di [MSAck](https://www.autistici.org/mailman/listinfo/msack),
vuole restituire quel percorso a studenti e non solo.

# Contatti
- ~~Comunichiamo principalmente sul canale IRC #pihack di [A/I](https://www.autistici.org/docs/irc/).~~
- ~~Canale XMPP: PiHack@chat.disroot.org~~
- Abbiamo un canale Telegram: https://t.me/Pi_Haqk  
  Unisciti per seguire e partecipare alle nostre discussioni.

# Prossimi eventi

# Eventi passati
- 2022-03-29
[Workshop LaTeX](https://roamresearch.com/#/app/ponys/page/YEQK5OP50), 
[FB](https://www.facebook.com/ponys.unina/posts/2979037445742492 "Facebook"),
[IG](https://www.instagram.com/p/Cb2FkXlohDf "Instagram")

# Le settimane del pihack
Pihack propone delle settimane di approfondimento di argomenti, programmi
e servizi digitali.

## Settimana in corso
Periodo di meditazione estiva.

## Settimane svolte
- 28/2/2022 - 7/2/2022 : Settimana di [Nyxt](nyxt.md)
- 25/4/2022 - 29/4/2022 : Settimana di pass

# Articoli
PiHack propone articoli di approfondimento su diversi argomenti di interesse.
